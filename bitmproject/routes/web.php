<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('admin')->group(function () {
    Route::get('/','AdminController@index');
    Route::prefix('categories')->group(function () {
        Route::get('/','AdminCategoriesController@index')->name('categories.index');
        Route::get('/create','AdminCategoriesController@create')->name('categories.create');
        Route::post('/','AdminCategoriesController@store')->name('categories.store');
    
    });
});
