@extends('admin.layouts.master')
@section('page_title','Categories/Add')
@section('content')
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Category Add
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        @if ($message = Session::get('success'))

                        <div class="alert alert-success">

                            <p>{{ $message }}</p>

                        </div>

                        @endif
                        <form role="form" action="{{ route('categories.store') }}" method="POST">
                        {{csrf_field()}}
                            <div class="form-group">
                                <label>Category</label>
                                <input name="catname" class="form-control" placeholder="Enter text">
                            </div>
                            <button type="submit" class="btn btn-default">Save</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection