<?php

namespace App\Http\Controllers;

use App\Admincategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class AdminCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.categories.index');
        //return view('admin.categories.index',compact('admincategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //dd($request->catname);
        request()->validate([
            'catname' => 'required',
        ]);
        Admincategory::create($request->all());
        return redirect()->route('categories.create')
                        ->with('success', 'Category created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admincategory  $admincategory
     * @return \Illuminate\Http\Response
     */
    public function show(Admincategory $admincategory)
    {
        return view('admin.categories.index',compact('admincategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admincategory  $admincategory
     * @return \Illuminate\Http\Response
     */
    public function edit(Admincategory $admincategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admincategory  $admincategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admincategory $admincategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admincategory  $admincategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admincategory $admincategory)
    {
        //
    }
}
