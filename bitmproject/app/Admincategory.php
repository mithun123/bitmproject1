<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admincategory extends Model
{
    //
    
        protected $table='categories';
        protected $fillable = ['catname'];
}
